I've seen a use of a TreeSet when instantiating a Set and I couldn't figure out why, when everyone else seems to be using HashSet.

I've tested the execution times of Set methods on HashSet, LinkedHashSet and TreeSet iplementations.
Results seem to be in line with documentation that mentions O(1) operations for HashSet and O(log(n)) for TreeSet

Conclusion TreeSet is good for its NavigableSet interface, not for it's Set one. 
When just wanting to work with a set, go for HashSet or LinkedHashSet unless you need it sorted, in which case go for TreeSet.