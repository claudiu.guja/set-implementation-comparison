package me.gujac.set_implementation_comparrison.config;

import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;

@Getter
public class ConfigLoader {

  private final SizeConfig sizeConfig;

  public ConfigLoader() {
    this.sizeConfig = ConfigBeanFactory.create(ConfigFactory.load(), SizeConfig.class);
  }
}
