package me.gujac.set_implementation_comparrison.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.util.Objects.isNull;

@NoArgsConstructor
@Getter
@Setter
public class SizeConfig {

  private Integer setSize;
  private Double searchSetSizeRatio;
  private Integer searchSetSize;

  public Integer getSearchSetSize() {
    if (isNull(searchSetSize)) {
      searchSetSize = (int) (setSize * searchSetSizeRatio);
    }
    return searchSetSize;
  }
}
