package me.gujac.set_implementation_comparrison;

import me.gujac.set_implementation_comparrison.config.ConfigLoader;
import me.gujac.set_implementation_comparrison.config.SizeConfig;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Math.random;
import static java.lang.System.nanoTime;

public class Main {

  private static final String HASH_SET = "HashSet";
  private static final String TREE_SET = "TreeSet";
  private static final String LINKED_HASH_SET = "LinkedHashSet";

  public static void main(String[] args) {
    SizeConfig config = new ConfigLoader().getSizeConfig();

    Collection<UUID> elementsToAdd = getUnorderedRandomElements(config.getSetSize());

    Collection<UUID> elementsToSearchForIncludedInSet =
        getContainedUnorderedElements(elementsToAdd, config.getSearchSetSizeRatio());

    List<UUID> elementsToSearchForNotIncludedInSet =
        getNotContainedUnorderedElements(config.getSearchSetSize());

    Map<Long, String> orderOfExecution = new TreeMap<>();

    Map<String, Set<UUID>> testStructures = new HashMap<>();
    testStructures.put(HASH_SET, new HashSet<>());
    testStructures.put(TREE_SET, new TreeSet<>());
    testStructures.put(LINKED_HASH_SET, new LinkedHashSet<>());

    System.out.println("-----Add elements to set-----");
    testStructures
        .entrySet()
        .forEach(e -> testAddAll(e, elementsToAdd, orderOfExecution, config.getSetSize()));
    printAndResetOrderOfExecution(orderOfExecution);

    System.out.println("-----Search for contained elements-----");
    testStructures
        .entrySet()
        .forEach(e -> testContains(e, elementsToSearchForIncludedInSet, orderOfExecution));
    printAndResetOrderOfExecution(orderOfExecution);

    System.out.println("-----Search for not contained elements-----");
    testStructures
        .entrySet()
        .forEach(e -> testContains(e, elementsToSearchForNotIncludedInSet, orderOfExecution));
    printAndResetOrderOfExecution(orderOfExecution);

    Map<String, Set<UUID>> testStructuresCopy = new HashMap<>();
    testStructuresCopy.put(HASH_SET, new HashSet<>(testStructures.get(HASH_SET)));
    testStructuresCopy.put(TREE_SET, new TreeSet<>(testStructures.get(TREE_SET)));
    testStructuresCopy.put(
        LINKED_HASH_SET, new LinkedHashSet<>(testStructures.get(LINKED_HASH_SET)));

    System.out.println("-----Remove contained elements-----");
    testStructures
        .entrySet()
        .forEach(e -> testRemove(e, elementsToSearchForIncludedInSet, orderOfExecution));
    printAndResetOrderOfExecution(orderOfExecution);

    System.out.println("-----Remove not contained elements-----");
    testStructuresCopy
        .entrySet()
        .forEach(e -> testRemove(e, elementsToSearchForNotIncludedInSet, orderOfExecution));
    printAndResetOrderOfExecution(orderOfExecution);
  }

  private static Collection<UUID> getUnorderedRandomElements(int size) {
    return Stream.generate(UUID::randomUUID).limit(size).collect(Collectors.toList());
  }

  private static Collection<UUID> getContainedUnorderedElements(
      Collection<UUID> collection, Double sizeFactor) {
    return collection.parallelStream()
        .filter(x -> random() < sizeFactor)
        .collect(Collectors.toList());
  }

  private static List<UUID> getNotContainedUnorderedElements(Integer size) {
    return Stream.generate(UUID::randomUUID).limit(size).collect(Collectors.toList());
  }

  private static <I> long measureTimeInNanosForElement(Consumer<I> consumer, I input) {
    long startTime = nanoTime();
    consumer.accept(input);
    long endTime = nanoTime();
    return endTime - startTime;
  }

  private static <I> long measureTimeInNanosForCollections(
      Consumer<I> consumer, Collection<I> input) {
    long startTime = nanoTime();
    input.forEach(consumer);
    long endTime = nanoTime();
    return endTime - startTime;
  }

  private static void printAndResetOrderOfExecution(Map<Long, String> orderOfExecution) {
    Map<Double, String> weightedOrderOfExecution = new TreeMap<>();
    Double shortestDuration = Double.valueOf(orderOfExecution.keySet().stream().findFirst().get());
    orderOfExecution.forEach((k, v) -> weightedOrderOfExecution.put(k / shortestDuration, v));
    System.out.print("Order of execution: ");
    weightedOrderOfExecution.forEach((key, value) -> System.out.print(value + ": " + key + " "));
    orderOfExecution.clear();
    System.out.print("\n\n\n");
  }

  private static void testAddAll(
      Map.Entry<String, Set<UUID>> setEntry,
      Collection<UUID> elementsToAdd,
      Map<Long, String> orderOfExecution,
      Integer setSize) {
    long duration = measureTimeInNanosForElement(setEntry.getValue()::addAll, elementsToAdd);
    System.out.printf(
        "%15s insertion of %d elements takes %10d nanoseconds\n",
        setEntry.getKey(), setSize, duration);
    orderOfExecution.put(duration, setEntry.getKey());
  }

  private static void testContains(
      Map.Entry<String, Set<UUID>> setEntry,
      Collection<UUID> elementsToSearch,
      Map<Long, String> orderOfExecution) {
    long duration =
        measureTimeInNanosForCollections(setEntry.getValue()::contains, elementsToSearch);
    System.out.printf(
        "%15s search of %d elements takes %10d nanoseconds\n",
        setEntry.getKey(), elementsToSearch.size(), duration);
    orderOfExecution.put(duration, setEntry.getKey());
  }

  private static void testRemove(
      Map.Entry<String, Set<UUID>> setEntry,
      Collection<UUID> elementsToSearch,
      Map<Long, String> orderOfExecution) {
    long duration = measureTimeInNanosForElement(setEntry.getValue()::removeAll, elementsToSearch);
    System.out.printf(
        "%15s removal of %d elements takes %10d nanoseconds\n",
        setEntry.getKey(), elementsToSearch.size(), duration);
    orderOfExecution.put(duration, setEntry.getKey());
  }
}
